# actev experiment-cleanup

## Description

Close any servers, terminates cluster (future functionality), etc.

The command contains the following subcommands:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|----------|----------------------------|
| system-cache-dir       | s | False    | path to system cache directory       |

## Usage

```
actev experiment-cleanup
```
