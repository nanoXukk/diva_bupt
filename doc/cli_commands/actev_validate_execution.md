# actev validate-execution

## Description

Test the execution of the system on each validation data set provided in container_output directory.

The command contains the following subcommands:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| output         | o | True    | path to experiment output json file    |
| reference      | r | True    | path to reference json file            |
| file-index     | f | True    | path to file index json file           |
| activity-index | a | True    | path to activity index json file       |
| result         | R | True    | path to result of the ActEV scorer     |   


## Usage

Generic command:

```
actev validate-execution -o <path to output result> -r <path to reference file> -a <path to activity> -f <path to file> -R <path to scoring result>
```

Example:

```
actev validate-execution ~/output.json -r diva_evaluation_cli/container_output/dataset/output.json -a ~/activity.json -f ~/file.json -R ~/result.json
```


