# actev get-system

## Description

Downloads a credentialed, web-accessible content into `/src`.

The command contains the following subcommands:

## get-system git
### Description

Clones a git repository

### Parameters

| Name      | Id | Required        | Definition                 |
|-----------|----|-----------------|----------------------------|
| url       | u  | True        | url to get the system      |
| location  | l  | False       | path to store the system   |
| user      | U  | False       | username to access the url |
| password  | p  | False       | password to access the url |
| token     | t  | False       | token to access the url    |

### Usage

Generic command:

```
actev get-system git -u <git repository>
```

With username and password: 

```
actev get-system git -u <git repository url> -U <username> -p <password>
```

With a token: 

```
actev get-system git -u <git repository url> -t <token>
```

Store the system in a specific directory:

```
actev get-system git -u <git repository url> ... -l <location>
```

:information_source: You can also directly add your credentials inside the url.

:warning: if your password or token starts with *-* as in this example: `-9r45ijFo0`, you should write `--password=-9r45ijFo0` instead of `-p -9r45ijFo0`. Otherwise, the value will be interpreted as an argument to parse.


Example:

```
actev get-system git -u https://gitlab.kitware.com/alexandreB/diva_evaluation_cli.git -l /tmp
```

## get-system docker
### Description

Downloads a docker image

### Parameters

| Name      | Id  | Required       | Definition                 |
|-----------|-----|----------------|----------------------------|
| url       | u   | True           | url to get the system      |
| user      | U   | False          | username to access the url |
| password  | p   | False          | password to access the url |

### Usage

Generic command:

```
actev get-system docker -u <docker image url>
```

With username and password: 

```
actev get-system docker -u <docker image url> -U <username> -p <password>
```

:warning: if your password starts with *-* as in this example: `-9r45ijFo0`, you should write `--password=-9r45ijFo0` instead of `-p -9r45ijFo0`. Otherwise, the value will be interpreted as an argument to parse.

Example:

```
actev get-system docker -u gitlab.kitware.com:4567/diva-baseline/diva-baseline:eval_cli
```

## get-system other
### Description

Downloads a web resources as a zip/tar file

### Parameters

| Name      | Id | Required        | Definition                 |
|-----------|----|-----------------|----------------------------|
| url       | u  | True            | url to get the system      |
| location  | l  | False           | path to store the system   |
| user      | U  | False           | username to access the url |
| password  | p  | False           | password to access the url |
| token     | t  | False           | token to access the url    |

### Usage

Generic command:

```
actev get-system other -u <web resource url>
```

With username and password: 

```
actev get-system other -u <web resource url> -U <username> -p <password>
```

With a token: 

```
actev get-system git -u <web resource url> -t <token>
```

Store the system in a specific directory:

```
actev get-system git -u <web resource url> ... -l <location>
```

:information_source: You can also directly add your credentials inside the url.

:warning: if your password or token starts with *-* as in this example: `-9r45ijFo0`, you should write `--password=-9r45ijFo0` instead of `-p -9r45ijFo0`. Otherwise, the value will be interpreted as an argument to parse.


Example:

```
actev get-system other -u https://path/to/file.tgz -l /tmp
```
