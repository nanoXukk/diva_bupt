# actev post-process-chunk

## Description

Post process a chunk

The command contains the following subcommands:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| chunk-id            | i | True    | chunk id                         |
| system-cache-dir    | s | False    | path to system cache directory   |


## Usage

Generic command:

```
actev post-process-chunk -i <id of a chunk> -s <path to system cache directory>
```

Example:

```
actev post-process-chunk -i Chunk1 -s ~/system_cache_dir
```