# actev experiment-init

## Description

Start servers, starts cluster, etc.

The command contains the following subcommands:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| file-index         | f | True    | path to file index json file       |
| activity-index     | a | True    | path to activity index json file   |
| chunks             | c | True    | path to chunks json file           |
| nb-video-per-chunk | n | False   | number of videos in the chunk      |
| video-location     | v | True    | path to videos content             |   
| system-cache-dir   | s | True    | path to system cache directory     |
| config-file        | C | False   | path to config file                |


## Usage

Generic command:

```
actev experiment-init -f <path to file.json> -a <path to activity.json> -c <path to chunks.json > -v <path to videos directory> -s <path to system cache directory>
```

Example:

```
actev experiment-init -f ~/file.json -a ~/activity.json -c ~/chunks.json -v ~/videos/ -s ~/system_cache_dir/
```

