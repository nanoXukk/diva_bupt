# actev validate-execution

## Description

Checks the structure of the  directory after ActEV-system-setup is run. Checks for expected API contents, etc.

The command contains the following subcommands:

## Usage

```
actev validate-system
```

