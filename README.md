Introduction
============

Consult the documentation to learn more about the CLI: [introduction](doc/introduction.md)

This repository contains:
* an abstract CLI to implement on the `master` branch 
* an implementation example on the branch `baseline_system_master`. 

You should fork the project and use the `master` branch in order to implement the entry point methods and get the updates.

Test it
============

Get the CLI
-----------

**Requirements: python3.X**

Clone the repository:
```
git clone https://gitlab.kitware.com/nanoXukk/diva_bupt.git
```

Install it
----------

* Go into the clone of this repository:

```
$ cd diva_bupt
```

* Execute the following script:

```
$ diva_evaluation_cli/bin/install.sh
```

Test the installation
---------------------

Run the following command:

```
$ actev
```

You should be able to see the available subcommands.


Usage
=====

Command line
------------

Run the following command to obtain help with the CLI:

```
$ actev -h
```

Documentation
-------------

Consult the documentation to have information: [CLI](doc/cli_commands/index.md)


Fork it and develop your own implementation
===========================================

Fork it
-------

Click on the “Fork” button to make a copy of the repository in your own space and add this repository as a remote upstream to get the latest updates

```
$ git remote add upstream https://gitlab.kitware.com/alexandreB/diva_evaluation_cli.git
```

Update it
---------

Execute the following commands to update it:

```
$ git fetch upstream
$ git checkout master
$ git rebase upstream/master
```

Develop it
----------

The CLI components are included in the `bin` directory, you do not have to modify it. 
To add your code, you simply have to implement the methods in `src/entry_points`. 

We suggest you to call your scripts in the entry point methods and store them in `src`. 
More information about the development and the update of the CLI here: [development](doc/development.md)

Contact
=======

