1.1.5 - 03.04.19
================

* Remove get-system subcommand: docker
* Rename get-system 'other' subcommand into 'archive'
* Improve get-system 'archive' and 'git' subcommands

1.1.4 - 02.22.19
================

* Bug fixes
* Add a new command: actev clear-logs

1.1.3 - 02.06.19
================

* Bug fixes

1.1.2 - 11.21.18
================

* Add a new data set: ActEV Validation Set1
* Bug fixes

1.1.1 - 11.20.18
================

* Add a new feature: resource monitoring
* Add some comments in the entry points
* Bug fixes

1.1 - 11.16.18
==============

* Add a new command: actev validate-execution
* Complete documentation
* Modify installation script: add requirements installation 

1.0.3 - 11.09.18
================

* Add a new command: actev status
* Add documentation about actev status
* Fix a bug in actev exec when nb_videos_per_chunk was missing
* Add actions before and after command: check video files validity in experiment-init

1.0.2 - 10.26.18
================

* Fix the installation script: support python virtual environments
* Modify the installation section of the README
* Add a new argument to pre/post/process-chunk: --system-cache-dir

1.0.1 - 10.24.18
================

* Add a new argument to reset-chunk: --system-cache-dir
* Modify names of the arguments composed of multiple words. Example: --chunk_id becomes --chunk-id

1.0 - 10.23.18
==============

* Release the abstract CLI: available on the master branch
* Release a baseline system CLI implementation: available on the baseline_system_master branch

