
import os
from multiprocessing import Pool, Queue, Manager
import json
import pynvml

def docker_worker(args):
    q, video_location, video_name, res_save = args
    gpu_id = q.get()
    cmd = 'docker run --shm-size=16g --runtime nvidia -it --rm \
            -v {}:/workspace/data/videos \
            -v {}:/workspace/data/res \
            -e NVIDIA_VISIBLE_DEVICES={} \
            actev_v3 \
            /bin/bash -c "/workspace/run.sh {}"'.format(video_location, res_save, gpu_id, video_name)

    os.system(cmd)

    q.put(gpu_id)


def process_chunk(jsonpath, video_location):
    files = None
    with open(jsonpath, 'r') as f:
        chunk = json.load(f)
        assert len(list(chunk.keys())) == 1, "chunk number should be 1 !"
        key = list(chunk.keys())[0]
        files = list(chunk[key]["files"])
    assert files is not None, "files to be processed should not be None!"
    res_save = os.path.abspath(os.path.dirname(os.path.abspath(jsonpath)))
    res_save = os.path.join(res_save, "CHUNK_RES")
    if os.path.exists(res_save):
        os.system("rm -rf {}".format(res_save))
    os.makedirs(res_save)

    pynvml.nvmlInit()
    gpu_num = pynvml.nvmlDeviceGetCount()
    manager = Manager()
    q = manager.Queue()

    for i in range(gpu_num):
        q.put(i)

    pool = Pool(gpu_num)
    args = [(q, video_location, video_name, res_save) for video_name in files]
    pool.map(docker_worker, args)
