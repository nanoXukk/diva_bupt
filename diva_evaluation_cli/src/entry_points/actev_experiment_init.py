"""Entry point module: experiment-init

Implements the entry-point by using Python or any other languages.

"""
import os

def entry_point(file_index, activity_index, chunks,
    video_location, system_cache_dir, config_file=None):
    """Method to complete: you have to raise an exception if an error occured in the program.

    Start servers, starts cluster, etc.

    Args:
        file_index(str): Path to file index json file for test set
        activity_index(str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        config_file (str, optional): Path to config file

    """
    # go into the right directory to execute the script
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, '../data_mcprl')
    os.makedirs(path, exist_ok=True)
    with open(os.path.join(path, "video_location.txt"), 'w') as f:
        f.write(os.path.abspath(video_location))

    path = os.path.dirname(__file__)
    script = os.path.join(path, '../implementation/init_experiment.sh')
    
    status = os.system(script)
    if status != 0:
        raise Exception("Error occured in init_experiment.sh")
